// Now I have to create a variable for each of 
// the elements we want to be able to interact with or manipulate in some way in our HTML

//Assigning its value to the result of a function
const drinksElement = document.getElementById("drinks");
const priceElement = document.getElementById("price");
const addElement = document.getElementById("add");
const cartElement = document.getElementById("cart");
const quantityElement = document.getElementById("quantity");
const payButtonElement = document.getElementById("pay");
const totalDueElement = document.getElementById("totalDue");

// The state of the 
let drinks = [];
let cart = [];
let totalDue = 0.0;

fetch("noroff-accelerate-drinks.herokuapp.com/drinks")
    .then(res => res.json())
    .then(data => drinks = data.data)
    .then(drinks => addDrinksToMenu(drinks))

const addDrinksToMenu = (drinks) => {
    drinks.forEach(x => addDrinkToMenu(x));
}

const addDrinkToMenu = (drink) => {
    const drinkElement = document.createElement("option");
    drinkElement.value = drink.id;
    drinkElement.appendChild(document.createTextNode(drink.description));
    drinksElement.appendChild(drinkElement);
}